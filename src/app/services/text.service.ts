import { Injectable } from '@angular/core';
import { WINE } from './wine.dict';
import { WHITMAN } from './whitman.dict';
import { CONAN } from './conan.dict';
import { METAMORPHOSIS } from './metamorphosis.dict';
import { SCIENCE } from './science.dict';
import { SOFTWARE } from './software.dict';
import { TERMS } from './terms.dict';

@Injectable()
export class TextService {

    public sourceSentences: string[];
    public output: string[];
    public sourceSelection = 'wine';

    constructor() {
        this.resetSource();
    }

    resetSource() {

        // copy fresh set from database
        if (this.sourceSelection === 'wine') {
            this.sourceSentences = WINE.slice(0);

        } else if (this.sourceSelection === 'whitman') {
            this.sourceSentences = WHITMAN.slice(0);

        } else if (this.sourceSelection === 'conan') {
            this.sourceSentences = CONAN.slice(0);

        } else if (this.sourceSelection === 'kafka') {
            this.sourceSentences = METAMORPHOSIS.slice(0);
        
        } else if (this.sourceSelection === 'science') {
            this.sourceSentences = SCIENCE.slice(0);
        
        } else if (this.sourceSelection === 'software') {
            this.sourceSentences = SOFTWARE.slice(0);
        
        } else if (this.sourceSelection === 'eula') {
            this.sourceSentences = TERMS.slice(0);
        
        } else {
            this.sourceSentences = WINE.slice(0);
        }
    }

    genSentence() {

        // ran out of sentences? reset source
        if (this.sourceSentences.length === 0) {
            this.resetSource();
        }

        // return a sentence and remove it from source
        const i = Math.floor(Math.random() * this.sourceSentences.length);
        const selected = this.sourceSentences[i];
        this.sourceSentences.splice(i, 1);
        return selected;

    }

    genParagraphs( numParas: number, minSens: number, maxSens: number, source: any) {

        this.sourceSelection = source;
        this.resetSource();

        this.output = [];

        let currentParas = 0;

        while ( currentParas < numParas ) {

            // pick a random number of sentences
            const numSentences = Math.floor(Math.random() * maxSens) + minSens;
            let currentSentences = 0;
            let newPara = '';

            // add a few sentences
            while ( numSentences > currentSentences ) {
                newPara = newPara + this.genSentence() + ((numSentences === currentSentences + 1 ) ? '' : ' ');
                currentSentences = currentSentences + 1;
            }

            this.output.push(newPara);

            currentParas = currentParas + 1;

        }

        return this.output;
    }

}



