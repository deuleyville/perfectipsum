import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  darkMode = false;

  constructor() {
    if (localStorage.getItem('darkMode') !== null) {
      this.darkMode = localStorage.getItem('darkMode') === 'true' ? true : false;
    }

  }

  toggleColorMode() {
    this.darkMode = this.darkMode ? false : true;
    localStorage.setItem('darkMode', this.darkMode === true ? 'true' : 'false');
  }

}
